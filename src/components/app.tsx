import React, { ReactElement, FC } from 'react';

const App: FC = (): ReactElement => {
  return <h1>Hello World</h1>;
};

export default App;
