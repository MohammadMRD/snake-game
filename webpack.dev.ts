import webpack from 'webpack';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import path from 'path';

const htmlPlugin = new HtmlWebpackPlugin({
  template: path.resolve(__dirname, 'src/index.html'),
});

const config: webpack.Configuration = {
  mode: 'development',
  entry: path.resolve(__dirname, 'src/index.tsx'),
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.jsx', '.json'],
  },
  module: {
    rules: [{ test: /\.tsx?$/, loader: 'awesome-typescript-loader' }],
  },
  plugins: [htmlPlugin],
};

export default config;
